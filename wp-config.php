<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //

if(file_exists(dirname(__FILE__) . '/local.php')) {

// Local database settings

define( 'DB_NAME', 'local' );
define( 'DB_USER', 'root' );
define( 'DB_PASSWORD', 'root' );
define( 'DB_HOST', 'localhost' );

}
else {
	// Live database
define( 'DB_NAME', 'u792401321_cjdb' );
define( 'DB_USER', 'u792401321_admin' );
define( 'DB_PASSWORD', 'cecaniah' );
define( 'DB_HOST', 'localhost' );

}




define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'bR3gztFF8o5tKy1e+Vat4lbsDRc6pYS/QQiYgP4EwCyiAf6+wHHpTqLLB8sbgykHQuSu5YCw8aVGUrvFQskbkg==');
define('SECURE_AUTH_KEY',  '2XCYKPaTp6SkZvBu2tgnN40svGPDXg6f5LvPCe+yMsUKel2TW6HCmNGvHgenJuLVHjXrIhWrkR5RYuEmBwJ2Wg==');
define('LOGGED_IN_KEY',    'fdyjT9xX/6xpGIsDIiuEQqnaYPuogESnAk8cI596UhtRYu5pkpbo3AP45JZRzjJ+mPXE0PPR1gzEi2NizQMfZg==');
define('NONCE_KEY',        '9x5SzdcoQnro12rls29WAI9MpPSu8AiszjqL8FSAE3V2/HKmyapgsOQcHD9FVGc7B1axNe1rU9goc83Hrv0k2w==');
define('AUTH_SALT',        'Z6CufobNXjqUTS8Rjc9Vk39sOwG2EBzMtVF+JjrWgC2LsxLCDtk80xOaBg6gVjPQnZw/k/7GQ9huD0t9IiapnQ==');
define('SECURE_AUTH_SALT', '/vIPnqOLF8W3EBOmCor6udlz+7Fv/Qv3612dmUB7l/YiU83HjeeAaM8tC8b7jRVmUDlo2SqZVcvqbhVUDnG96g==');
define('LOGGED_IN_SALT',   'jFP20+E8utzv2BSY783x4039pov0h7s/RmJnJ4WdhuBOkps7jmwwWl2UHmU3jdtbswWLIH0Wk0AUu0tt3Wp1og==');
define('NONCE_SALT',       'jC3I8ESjbKY3F8bZgbOdllTemK9qdaG7rdBoRahpFtpqSF98HD2bG8xz8QzZ90X2kG+WifIlF1i8q3sYk7NPBg==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
