<?php

function cl_post_types () {

	// Portfolio Post Type
	register_post_type('portfolio', array(
		'has_archive' => true,
		'rewrite' => array('slug' => 'portfolios'),
		'supports' => array('title', 'editor'),

		'public' => true,
		'labels' => array(
			'name' => 'Portfolios',
			'add_new_item' => 'Add New Portfolio',
			'edit_item' => 'Edit Portfolio',
			'all_items' => 'All Portfolios',
			'singular_name' => 'Portfolio'
		),
		'menu_icon' => 'dashicons-welcome-widgets-menus'
	));

	// Services
	register_post_type('service', array(
		'has_archive' => true,
		'rewrite' => array('slug' => 'services'),
		'supports' => array('title', 'editor'),

		'public' => true,
		'labels' => array(
			'name' => 'Services',
			'add_new_item' => 'Add New Service',
			'edit_item' => 'Edit Service',
			'all_items' => 'All Services',
			'singular_name' => 'Service'
		),
		'menu_icon' => 'dashicons-portfolio'
	));

	// Facts
	// register_post_type('fact', array(
	// 	'has_archive' => true,
	// 	'rewrite' => array('slug' => 'facts'),
	// 	'supports' => array('title', 'editor'),

	// 	'public' => true,
	// 	'labels' => array(
	// 		'name' => 'Facts',
	// 		'add_new_item' => 'Add New Fact',
	// 		'edit_item' => 'Edit Fact',
	// 		'all_items' => 'All Facts',
	// 		'singular_name' => 'Fact'
	// 	),
	// 	'menu_icon' => 'dashicons-archive'
	// ));


	// For Skill Bar
	// 	register_post_type('skill', array(
	// 	'has_archive' => true,
	// 	'rewrite' => array('slug' => 'skills'),
	// 	'supports' => array('title', 'editor'),
	// 	'public' => true,
	// 	'labels' => array(
	// 		'name' => 'Skills',
	// 		'add_new_item' => 'Add New Skills',
	// 		'edit_item' => 'Edit Skill',
	// 		'all_items' => 'All Skills',
	// 		'singular_name' => 'Skill'
	// 	),
	// 	'menu_icon' => 'dashicons-chart-bar'
	// ));

	// For Timeline Bar
		register_post_type('timeline', array(
		'has_archive' => true,
		'rewrite' => array('slug' => 'timelines'),
		'supports' => array('title'),
		'public' => true,
		'labels' => array(
			'name' => 'Timelines',
			'add_new_item' => 'Add New Timelines',
			'edit_item' => 'Edit Timeline',
			'all_items' => 'All Timelines',
			'singular_name' => 'Timeline'
		),
		'menu_icon' => 'dashicons-calendar'
	));

	// // For Timeline Bar
	// 	register_post_type('contact', array(
	// 	'has_archive' => true,
	// 	'rewrite' => array('slug' => 'contacts'),
	// 	'supports' => array('title'),
	// 	'public' => true,
	// 	'labels' => array(
	// 		'name' => 'Contacts',
	// 		'add_new_item' => 'Add New Contacts',
	// 		'edit_item' => 'Edit Contact',
	// 		'all_items' => 'All Contacts',
	// 		'singular_name' => 'Contact'
	// 	),
	// 	'menu_icon' => 'dashicons-phone'
	// ));


	// // Professor Post Type
	// register_post_type('professor', array(
	// 	// 'has_archive' => true,
	// 	// 'rewrite' => array('slug' => 'professors'),
	// 	'show_in_rest' => true,
	// 	'supports' => array('title','editor','thumbnail'),
	// 	'public' => true,
	// 	'labels' => array(
	// 		'name' => 'Professors',
	// 		'add_new_item' => 'Add New Professor',
	// 		'edit_item' => 'Edit Professor',
	// 		'all_items' => 'All Professors',
	// 		'singular_name' => 'Professor'
	// 	),
	// 	'menu_icon' => 'dashicons-welcome-learn-more'
	// ));

	// // Campus Post Type
	// register_post_type('campus', array(
	// 	'capability_type' => 'campus',
	// 	'map_meta_cap' => true,
	// 	'has_archive' => true,
	// 	'rewrite' => array('slug' => 'campuses'),
	// 	'supports' => array('title','editor','excerpt'),

	// 	'public' => true,
	// 	'labels' => array(
	// 		'name' => 'Campuses',
	// 		'add_new_item' => 'Add New Campus',
	// 		'edit_item' => 'Edit Campus',
	// 		'all_items' => 'All Campuses',
	// 		'singular_name' => 'Campus'
	// 	),
	// 	'menu_icon' => 'dashicons-location-alt'
	// ));



}


add_action('init', 'cl_post_types');

?>