


				<div class="col-lg-3 col-md-6 single-fact">

					<?php $args = get_the_title();
						  $content = get_the_content();

					if($args == "Projects Accomplished" OR $args == "Cups of Choco" OR $args == 'Satisfied Clients'): ?>
					<h1 class="counter span-plus"><?= $content ?></h1>
					<?php else: ?>
					<h1 class="counter"><?= $content ?></h1>
					<?php endif; ?>
					<p><?= the_title(); ?></p>
				</div>
