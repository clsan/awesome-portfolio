
<section class="home-about-area pt-30">
				<div class="container-fluid">
					<div class="row align-items-center justify-content-between">
						<div class="col-lg-5 col-md-5 home-about-left">
							<img class="img-fluid" src="<?= get_theme_file_uri('/img/candy-hero.png'); ?>" alt="Candy 3D Rendered Laptop Web Design and Graphic Design">
						</div>
						<div class="col-lg-7 col-md-7 home-about-right">
								<h1 class="text-uppercase"><?= get_the_title(); ?></h1>
								<p><?= the_content();?></p>
						</div>

					</div>
				</div>
</section>