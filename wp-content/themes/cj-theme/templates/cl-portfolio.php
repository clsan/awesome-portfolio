<?php
$category = get_field('portfolio_category'); ?>

<div class="single-portfolio col-sm-3 all <?php

	if(is_array($category)):
	echo implode(' ', $category);
	else:
	echo $category;
	endif; ?>

	">
	<div class="relative">
		<?= portfolioImage(); ?>

	</div>
	<div class="p-inner">
	    <h4><?= the_title(); ?></h4>
		<div class="cat">
			<?= the_content(); ?>

				<?php if(get_field('portfolio_link') != ''): ?>
					<p><a target="_blank" href="<?= get_field('portfolio_link')?>">View this project.</a></p>
				<?php else: ?>
					<p>No link available.</p>
				<?php endif; ?>
			</div>
	</div>
</div>
