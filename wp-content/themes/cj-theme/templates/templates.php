<!-- Toolbar and Skillbar -->

	<div class="container">
					<div class="row skillbar-wraps">

						<div class="col-lg-6 skill-left">
							<h4 class="pt-30">Skills Expertness</h4>
		<?php


		$skillbar = new WP_Query(
		array(
			'post_type' => 'skill',
			'skill_type' => 'Skills',
			'orderby' => 'title',
			'order' => 'ASC',
			'meta_query' => array(
				array(
					'key' => 'skill_type',
					'compare' => 'LIKE',
					'value' => 'Skills'
				))
		));


		while($skillbar->have_posts()):
			$skillbar->the_post();

		get_template_part('templates/cl-skillbar');

		endwhile;
		wp_reset_postdata();

		?>
					</div>

						<div class="col-lg-6 skill-right">
							<h4 class="pt-30">Tools Expertness</h4>
		<?php


		$toolbar = new WP_Query(
		array(
			'post_type' => 'skill',
			'skill_type' => 'Tools',
			'orderby' => 'ID',
			'order' => 'ASC',
			'meta_query' => array(
				array(
					'key' => 'skill_type',
					'compare' => 'LIKE',
					'value' => 'Tools'
				))
		));


		while($toolbar->have_posts()):
			$toolbar->the_post();

		get_template_part('templates/cl-toolbar');

		endwhile;
		wp_reset_postdata();

		?>

						</div>
						</div>

				</div>


<!-- FACT -->
	<section class="facts-area section-gap" id="facts-area">
		<div class="container">
			<div class="row">

				<?php

					$fact = new WP_Query(
						array(
							'posts_per_page' => 4,
							'post_type' => 'fact',
							'orderby' => 'title',
							'order' => 'ASC'
						));

				?>
				<?php

					while($fact->have_posts()):
						$fact->the_post();
				get_template_part('templates/cl-fact');

					endwhile;


				wp_reset_postdata();
				?>
			</div>
		</div>
	</section>