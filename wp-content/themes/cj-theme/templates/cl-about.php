
	<?php $uploads = wp_get_upload_dir(); ?>

<section class="home-about-area pt-120">
				<div class="container">
					<div class="row align-items-center justify-content-between">
						<div class="col-lg-6 col-md-6 home-about-left">
						<?php if(is_page('Home')): ?>
							<img class="img-fluid" src="<?= get_theme_file_uri('/img/about-hero.png'); ?>" alt="3D Torus Rendered Cinema 4D Laptop Web Design and Graphic Design">
						<?php else: ?>
							<img class="img-fluid" src="<?= get_theme_file_uri('/img/candy-hero.png'); ?>" alt="Candy 3D Rendered Laptop Web Design and Graphic Design">
						<?php endif; ?>
						</div>
						<div class="col-lg-5 col-md-6 home-about-right">

							<?php if(!is_page('Home')): ?>
								<h6><?= get_field('page_post_subtitle')?></h6>
								<h1 class="text-uppercase"><?= get_the_title(); ?></h1>
								<p><?= the_content();?></p>

							<?php else: ?>

							<h6>Who am i?</h6>
							<h1 class="text-uppercase">Hey there!</h1>
							<p>
								I'm a creative web designer/developer and digital designer with expertise in designing high standard visuals and responsive web & mobile designs.<br>
								<br>If you want to view some of my works you can visit my <a target="_blank" href="https://www.behance.net/cjluntok">Behance</a>. Feel free to say Hi!
							</p>
						<?php endif; ?>

							<?php if(is_page('Home')): ?>
							<a href="<?= site_url('/about'); ?>" class="primary-btn text-uppercase">View Full Details</a>
							<?php else: ?>
								<a target="_blank" href="<?= get_theme_file_uri("/files/cl-cv.pdf") ?>" class="primary-btn text-uppercase">View CV</a>
							<?php endif; ?>
						</div>
					</div>
				</div>
</section>