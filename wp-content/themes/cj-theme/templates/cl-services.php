
<?php $icon = get_field('services_icon'); ?>
						<div class="col-lg-4 col-md-6">
							<div class="single-services">
								<span class="lnr"><img class="services-svg"src="<?= get_theme_file_uri("/img/icons/$icon.svg")?>"></span>
								<h4><?= the_title(); ?></h4>
								<p>
									<?= the_content(); ?>
								</p>
							</div>
						</div>

