<?php

get_header();


PageBanner();

?>

<main id="main-container">

	<?php $headerpost = new WP_Query(
		array(
			'post_type' => 'post',
			'post_category' => 'About',
			'meta_query' => array(
				array(
					'key' => 'post_category',
					'compare' => 'LIKE',
					'value' => 'About'
				))
		));


		while($headerpost->have_posts()):
			$headerpost->the_post();

		get_template_part('templates/cl-about-rev');

		endwhile;

 		$headerpost->reset_postdata();
 		wp_reset_postdata();

 		$upload = wp_get_upload_dir();
 		?>

<section class="services-area section-gap-60">
		<div class="container">
			<div class="row d-flex justify-content-center">
				<div class="menu-content  col-lg-7">
					<div class="title text-center">
						<h1 class="mb-10">Skills</h1>
						<p>Here are my top notch skills to create digital design and products!</p>
					</div>
				</div>
			</div>
			<div class="row">
				<?php

				$services = new WP_Query(array(
					'posts_per_page' => 6,
					'post_type' => 'service',
					'meta_key' => 'services_icon',
					'orderby' => 'title',
					'order' => 'ASC',
				));
 				?>
				<?php
				while($services->have_posts()):
					$services->the_post();
				get_template_part('templates/cl-services');

				endwhile;

				wp_reset_postdata();
				?>
			</div>
		</div>
	</section>





		<section class="timeline pb-120">
            <div class="text-center">
                <div class="menu-content pb-70">
                    <div class="title text-center">
                        <h1 class="mb-10">Qualifications & Work Experiences</h1>
                        <p>Here is a timeline of my qualifications & work experiences.</p>
                    </div>
                </div>
            </div>
			  <ul>


		<?php

		$qualification = new WP_Query(
			array(
				'post_type' => 'timeline',
				'orderby' => 'ID',
				'order' => 'DESC'
			)
		);


		while($qualification->have_posts()):
			$qualification->the_post();

			get_template_part('templates/cb-timeline');

		endwhile;

		wp_reset_postdata();

		?>


			  </ul>
			</section>

		<?php get_template_part('templates/cb-contact-header'); ?>

</main>


<?php	get_footer(); ?>