<!DOCTYPE html>
<html <?= language_attributes();?> class="no-js">
<head>
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="author" content="Christian Luntok">
		<meta name="description" content="Personal Portfolio">
		<meta name="keywords" content="">
		<meta charset="<?= bloginfo('charset'); ?>">

	<?php // wordpress head functions ?>
	<?php wp_head(); ?>
	<?php // end of wordpress head ?>
<title><?php is_front_page() ? bloginfo('description') : wp_title(); ?></title>
</head>


<body <?= body_class() ?> >
		<?php get_template_part('templates/cl-loader'); ?>
		  <header id="header">

		    <div class="container main-menu">
		    	<div class="row align-items-center justify-content-between d-flex">

			      <div id="logo">
			        <h1><a href="<?= site_url(); ?>">Christian Luntok</a></h1>
			      </div>

			      <nav id="nav-menu-container">
			        <ul class="nav-menu">
			          <li><a href="<?= site_url(); ?>">Home</a></li>
			          <li><a href="<?= site_url('/about'); ?>">About</a></li>
			          <li><a href="<?= site_url('/portfolio'); ?>">Portfolio</a></li>
			          <li><a href="<?= site_url('/contact'); ?>">Contact</a></li>
			        </ul>
			      </nav>

		    	</div>
		    </div>

		  </header>