<?php

function everyPageBanner($args = NULL){

	print_r($args);

	if(!$args['title']) {
		$args['title'] = get_the_title();
	}


	if(!$args['subtitle']) {
		$args['subtitle'] = get_field('page_banner_subtitle');
	}



	?>
			<?php if(is_page('about')): ?>
			<section class="about-banner about-bg">
			<?php elseif(is_page('portfolio')): ?>
			<section class="about-banner portfolio-bg">
			<?php elseif(is_page('contact')): ?>
			<section class="about-banner contact-bg">
			<?php else: ?>
			<section class="about-banner">
			<?php endif; ?>
				<div class="container">
					<div class="row d-flex align-items-center justify-content-center">
						<div class="about-content col-lg-12">
							<h1 class="text-white">
								<?= $args['title'] ?>
							</h1>
							<p class="text-white link-nav"><?= $args['subtitle']?></p>
						</div>
					</div>

				</div>
			</section>

	<?php
}

?>