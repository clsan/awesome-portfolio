<?php
function portfolioImage ($args = NULL) {



if(!$args['image']) {

	if(get_field('portfolio_image')) {
		$args['image'] = get_field('portfolio_image');
		$args['thumbImage'] = get_field('portfolio_image')['sizes']['portfolioImage'];
	}
	else {
		$args['image'] = get_theme_file_uri('img/about-hero.png');
	}
	$portfolioTitle = $args['title'];
	$url = $args['image']['url'];
}

?>
<div class="thumb">
	<div class="overlay overlay-bg"></div>
	 <img class="image img-fluid" src='<?= $args['thumbImage'] ?>' alt="<?= $portfolioTitle;?>">

</div>
	<a href="<?= $args['image']['url']?>" class="img-pop-up">
  <div class="middle">
    <div class="text align-self-center d-flex"><img src="<?= get_theme_file_uri('img/icon-preview.png'); ?>" alt="<?=$args['image']['title']?>"></div>
  </div>
</a>

 <?php } ?>