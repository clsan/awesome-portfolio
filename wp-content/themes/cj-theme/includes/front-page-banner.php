<?php

function pageBanner($args = NULL) {

	if(!$args['title']) {
		$args['title'] = get_the_title();
	}

	if(!$args['subtitle']) {
		$args['subtitle'] = get_field('page_banner_subtitle');;
	}


	$uploads = wp_get_upload_dir();

?>

<?php if(is_page('about')): ?>
<section class="banner-area about-bg">
<?php elseif(is_page('portfolio')): ?>
<section class="banner-area portfolio-bg">
<?php elseif(is_page('contact')): ?>
<section class="banner-area contact-bg">
	<?php else: ?>
<section class="banner-area home-bg">
<?php endif; ?>
				<div class="container">
					<div class="row fullscreen align-items-center justify-content-between">
						<div class="col-lg-8 col-md-8 banner-left">
							<?php if(is_page('home')): ?>
							<h6>Hello, I'm</h6>
						<?php endif; ?>
							<h1><?= $args['title']?> </h1>
							<p><?= $args['subtitle']?>
							</p>

							<?php if(is_page('about')): ?>
							<p></p>
							<?php elseif(is_page('portfolio')): ?>
							<p></p>
							<?php elseif(is_page('contact')): ?>
							<p></p>
							<?php else: ?>
								<p class="printable"> Here's a printable version of my
								<a target="_blank" href="<?= get_theme_file_uri("/files/cl-portfolio.pdf") ?>" class="text-uppercase">Portfolio</a> and <a target="_blank" href="<?= get_theme_file_uri("/files/cl-cv.pdf") ?>" class="text-uppercase">CV.</a>
						<?php endif; ?>
						</div>
					</div>
				</div>
			</section>

<?php
}


?>