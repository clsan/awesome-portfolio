<?php

get_header();

pageBanner();

?>

<main id="main-container">

  	<?php
  		while(have_posts()):
  			the_post(); ?>
			<section class="home-about-area pt-120">
				<div class="container">
					<div class="row align-items-center justify-content-between">
						<div class="col-lg-5 col-md-6 home-about-right">
							<p>
								<?= the_content(); ?>
							</p>

						</div>
					</div>
				</div>
			</section>


<?php endwhile; ?>
</main>


<?php	get_footer(); ?>