<?php

get_header();


everyPageBanner();

?>

<main id="main-container">
  	<?php
  		while(have_posts()):
  			the_post(); ?>

			<section class="home-about-area pt-120">
				<div class="container">
					<div class="row align-items-center justify-content-between">
						<div class="col-lg-5 col-md-6 home-about-right">
							<p>
								<?= the_content(); ?>
							</p>

						</div>
					</div>
				</div>
			</section>

<?php endwhile;

	wp_reset_postdata();
?>

	<?php get_template_part('templates/cb-contact-header'); ?>
</main>


<?php	get_footer(); ?>