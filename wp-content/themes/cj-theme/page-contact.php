<?php

get_header();

PageBanner();

?>

<main id="main-container">
  	<section id="next" class="contact-page-area section-gap-60">
				<div class="container">
					<div class="row">
						<div class="col-lg-4 d-flex flex-column address-wrap">
							<div class="single-contact-address d-flex flex-row">
								<div class="icon">
									<span class="lnr lnr-select"></span>
								</div>

                        <div class="col-lg-6 col-md-6 col-sm-6 social-widget">
                             <div class="single-footer-widget">
                                <h4>Follow Me</h4>
                                <p>Follow me on these links!</p>
                                <div class="footer-social d-flex align-items-center pb-30">
                                    <a target="_blank" href="https://www.instagram.com/design.sef/"><i class="fab fa-instagram"></i></a>
                                    <a target="_blank" href="https://www.linkedin.com/in/christian-luntok/"><i class="fab fa-linkedin"></i></a>
                                    <a target="_blank" href="https://www.behance.net/cjluntok"><i class="fab fa-behance"></i></a>
                                </div>
                            </div>
                        </div>
							</div>
							<div class="single-contact-address d-flex flex-row">
								<div class="icon">
									<span class="lnr lnr-envelope"></span>
								</div>
								<div class="contact-details">
									<h5>design@cjluntok.com</h5>
									<p>Let's build something awesome!</p>
								</div>
							</div>
						</div>
						<div class="col-lg-8">
							<div class="text-left">
								<?= do_shortcode("[ninja_form id='1']"); ?>
							</div>
						</div>
					</div>
				</div>
			</section>

<?php

	wp_reset_postdata();
?>
</main>


<?php	get_footer(); ?>