<?php


get_header();

PageBanner();

?>

<main id="main-container">
	<section id="next">
  	<section class="portfolio-area section-gap-60" id="portfolio">
		<div class="container-fluid">
		            <div class="row d-flex justify-content-center">
		                <div class="menu-content pb-30 col-lg-8">
		                    <div class="title text-center">
		                        <h1 class="mb-10">All Projects</h1>
		                        <p>See 100% of my work, also check it out on <a target="_blank" href="https://www.behance.net/cjluntok">behance </a> and <a target="_blank" href="https://www.instagram.com/design.sef/">instagram</a>.</p>

		                    </div>
		                </div>
		            </div>

                    <div class="filters">
                        <ul>
                            <li class="active" data-filter="*">All</li>
                            <li data-filter=".illustration">Illustrations</li>
                            <li data-filter=".digital">Digital Design</li>
                            <li data-filter=".ui">UI/UX Design</li>
                            <li data-filter=".website">Websites</li>
                            <li data-filter=".commissioned">Commissioned Work</li>
                        </ul>
                    </div>

                    <div class="filters-content">
                    	<div id="portfolio-grid" class="row grid">
                    		<?php

								$portfolio = new WP_Query(
									array(
									'posts_per_page' => -1,
									'post_type' => 'portfolio',
									'meta_key' => 'portfolio_category',
									'order' => 'ASC',
									)
								);
                    		?>
                    		<?php

                    	while($portfolio->have_posts()):
							$portfolio->the_post();

							get_template_part('templates/cl-portfolio');

							endwhile;


					wp_reset_postdata();
					?>
                    	</div>
                    </div>
		</div>
	</section>
	</section>

	<?php get_template_part('templates/cb-contact-header'); ?>

</main>


<?php	get_footer(); ?>