<?php

require get_theme_file_path('/includes/front-page-banner.php');
require get_theme_file_path('/includes/portfolio-image.php');
require get_theme_file_path('/includes/every-page-banner.php');

// CL Personal Styles and Scripts

function cl_files () {
	$rand = rand(0,999);
// style for css , script for css
wp_enqueue_style('font-awesome', '//use.fontawesome.com/releases/v5.10.1/css/all.css');
wp_enqueue_style('custom-font','//fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700');
wp_enqueue_style('cl-linear-icons', get_theme_file_uri('/css/linearicons.css'), NULL, '1');
wp_enqueue_style('cl-bootstrap', get_theme_file_uri('/css/bootstrap.css'), NULL, '1');
wp_enqueue_style('cl-magnific-pop-up', get_theme_file_uri('/css/magnific-popup.css'), NULL, '1');
// wp_enqueue_style('cl-jquery-ui', get_theme_file_uri('/css/jquery-ui.css'), NULL, '1');
wp_enqueue_style('cl-nice-select', get_theme_file_uri('/css/nice-select.css'), NULL, '1');
wp_enqueue_style('cl-animate', get_theme_file_uri('/css/animate.min.css'), NULL, '1');
wp_enqueue_style('cl-owl-carousel', get_theme_file_uri('/css/owl.carousel.css'), NULL, '1');
wp_enqueue_style('cl_main_styles', get_stylesheet_uri(), NULL, $rand);


	wp_enqueue_script('cl-jquery', get_theme_file_uri('/js/vendor/jquery-2.2.4.min.js'), array(), 1.0, true);
	wp_enqueue_script('cl-popper-js', get_theme_file_uri('/js/popper.min.js'), array(), 1.0, true);
	wp_enqueue_script('cl-bootstrap-js', get_theme_file_uri('/js/vendor/bootstrap.min.js'), array(), 1.0, true);
	wp_enqueue_script('cl-easing-js', get_theme_file_uri('/js/easing.min.js'), array(), 1.0, true);
	wp_enqueue_script('cl-hoverintent-js', get_theme_file_uri('/js/hoverIntent.js'), array(), 1.0, true);
	wp_enqueue_script('cl-superfish-js', get_theme_file_uri('/js/superfish.min.js'), array(), 1.0, true);
	wp_enqueue_script('cl-jqueryajax-js', get_theme_file_uri('/js/jquery.ajaxchimp.min.js'), array(), 1.0, true);
	wp_enqueue_script('cl-manificpopup-js', get_theme_file_uri('/js/jquery.magnific-popup.min.js'), array(), 1.0,true);
	wp_enqueue_script('cl-jquerytabs-js', get_theme_file_uri('/js/jquery.tabs.min.js'), array(), 1.0, true);
	wp_enqueue_script('cl-niceselect-js', get_theme_file_uri('/js/jquery.nice-select.min.js'), array(), 1.0, true);
	wp_enqueue_script('cl-images-loaded', get_theme_file_uri('/js/imagesloaded.pkgd.min.js'), array(), 1.0, true);
	wp_enqueue_script('cl-isotope-js', get_theme_file_uri('/js/isotope.pkgd.min.js'), array(), 1.0, true);
	wp_enqueue_script('cl-waypoints-js', get_theme_file_uri('/js/waypoints.min.js'), array(), 1.0, true);
	wp_enqueue_script('cl-jquerycounterup-js', get_theme_file_uri('/js/jquery.counterup.min.js'), array(), 1.0, true);
	wp_enqueue_script('cl-simpleskillbar-js', get_theme_file_uri('/js/simple-skillbar.js'), array(), 1.0, true);
	wp_enqueue_script('cl-owlcarousel-js', get_theme_file_uri('/js/owl.carousel.min.js'), array(), 1.0,true);
	wp_enqueue_script('cl-main-script-js', get_theme_file_uri('/js/mail-script.js'), array(), 1.0, true);
	wp_enqueue_script('main-cl-js', get_theme_file_uri('/js/main.js'),
		array('jquery'), 3.0, true);



}


add_action('wp_enqueue_scripts','cl_files');






function cl_features() {
	add_theme_support('post-thumbnails');
	// 1st name, 2nd how wide, 3rd how tall , 4th if you want to crop
	add_image_size('portfolioImage', 1000, 1000, true);

}

add_action('after_setup_theme', 'cl_features');

?>