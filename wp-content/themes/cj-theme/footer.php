 <?php  $uploads = wp_get_upload_dir(); ?>
 <footer class="footer-area  align-items-center">
                <div class="container">
                    <div class="row justify-content-between">
                        <div class="col-lg-12 col-md-12 col-sm-6">
                            <div class="single-footer-widget">

                                <p class="footer-text">
Copyright &copy; 2018 - <script>document.write(new Date().getFullYear());</script> Christian Luntok. All rights reserved.</p>
<p class="footer-text d-none d-sm-none d-md-none d-lg-block">All graphics and images are all original and should not be downloaded and distributed without my permission.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>

<script type="text/javascript">


        let loader = document.querySelector('.loader');
        if(loader !== null) {
            window.addEventListener("load",() => {
                loader.classList.add('fadeout');
                setTimeout(function(){ loader.style.display = 'none'; }, 700)
            });
        }



</script>
<?php

wp_footer();

?>


</body>

</html>